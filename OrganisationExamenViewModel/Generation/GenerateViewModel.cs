﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.System.Threading;
using OrganisationExamenModel.Services.Scheduler;
using OrganisationExamenModel.Objects;
using OrganisationExamenViewModel.ObjectsViewModel;

namespace OrganisationExamenViewModel.Generation
{
    public class GenerateViewModel : ViewModelBase
    {
        public ICommand SeeGeneratePageCommand { get; set; }

        public string Result
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
                OnPropertyChanged("Result");

            }
        }
        
        public String result { get; set; }
        public GenerateViewModel()
        {
            SeeGeneratePageCommand = new RelayCommand(new Action(() => ChargePage())); 
        }

        public async void ChargePage()
        {
            DateTime start;
            DateTime.TryParse("18/02/2013 8:00:00", out start);
            DateTime end = new DateTime(start.Year, start.Month, start.Day + 4, 19, 0, 0);
            ClassRoomViewModel.Instance.ClassRooms = await ClassRoomViewModel.LoadClassRoom();
            TestViewModel.Instance.Tests = await TestViewModel.LoadTests();
            List<Test> tts = new List<Test>();
            List<ClassRoom> cms = new List<ClassRoom>();
            foreach (TestView test in TestViewModel.Instance.Tests)
            {
                Test t = new Test();
                t.Duration = test.Duration;
                t.Id = test.Id;
                t.Name = test.Name;
                t.Type = test.Type;
                t.Students = new List<Student>();
                tts.Add(t);
            }
            foreach (ClassRoomView view in ClassRoomViewModel.Instance.ClassRooms)
            {
                ClassRoom t = new ClassRoom();
                t.Capacity = view.Capacity;
                t.Designation = view.Designation;
                t.Num = view.Num;
                cms.Add(t);
            }
            Scheduler scheduler = new Scheduler(tts, cms, start, end);
            foreach (ClassRoom mroom in cms.ToList<ClassRoom>())
            {
                Result += mroom.ToString() + "\n";
            }
        }
        // Helper function to create pipe client processes
       /* private static void StartClients()
        {
            int i;
            string currentProcessName = Environment.CommandLine;
            Process[] plist = new Process[1];

            Console.WriteLine("Spawning client processes...\n");

            if (currentProcessName.Contains(Environment.CurrentDirectory))
            {
                currentProcessName = currentProcessName.Replace(Environment.CurrentDirectory, String.Empty);
            }

            // Remove extra characters when launched from Visual Studio
            currentProcessName = currentProcessName.Replace("\\", String.Empty);
            currentProcessName = currentProcessName.Replace("\"", String.Empty);

            for (i = 0; i < numClients; i++)
            {
                // Start 'this' program but spawn a named pipe client.
                plist[i] = Process.Start(currentProcessName, "spawnclient");
            }
            while (i > 0)
            {
                for (int j = 0; j < numClients; j++)
                {
                    if (plist[j] != null)
                    {
                        if (plist[j].HasExited)
                        {
                            Console.WriteLine("Client process[{0}] has exited.",
                                plist[j].Id);
                            plist[j] = null;
                            i--;    // decrement the process watch count
                        }
                        else
                        {
                            Thread.Sleep(250);
                        }
                    }
                }
            }
        }
    }

    // Defines the data protocol for reading and writing strings on our stream
    public class StreamString
    {
        private Stream ioStream;
        private UnicodeEncoding streamEncoding;

        public StreamString(Stream ioStream)
        {
            this.ioStream = ioStream;
            streamEncoding = new UnicodeEncoding();
        }

        public string ReadString()
        {
            int len;
            len = ioStream.ReadByte() * 256;
            len += ioStream.ReadByte();
            byte[] inBuffer = new byte[len];
            ioStream.Read(inBuffer, 0, len);

            return streamEncoding.GetString(inBuffer);
        }

        public int WriteString(string outString)
        {
            byte[] outBuffer = streamEncoding.GetBytes(outString);
            int len = outBuffer.Length;
            if (len > UInt16.MaxValue)
            {
                len = (int)UInt16.MaxValue;
            }
            ioStream.WriteByte((byte)(len / 256));
            ioStream.WriteByte((byte)(len & 255));
            ioStream.Write(outBuffer, 0, len);
            ioStream.Flush();

            return outBuffer.Length + 2;
        }*/
    }
}
