﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OrganisationExamenViewModel
{
    public class RelayCommand : ICommand
    {
        private Action<object> _action;
        private Action _actionWithoutParams;

        public RelayCommand(Action<object> action)
        {
            _action = action;
        }
        
        public RelayCommand(Action action)
        {
            _actionWithoutParams = action;
        }


        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            return true;
        }
        
        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (parameter != null)
            {
                if (_action != null)
                    _action(parameter);
                else if (_actionWithoutParams != null)
                    _actionWithoutParams();
            }
            else
            {
                if (_action != null)
                    _action("HelloWorld");
                else if (_actionWithoutParams != null)
                    _actionWithoutParams();
            }
        }

        #endregion
    }
}
