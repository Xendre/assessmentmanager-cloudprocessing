﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrganisationExamenModel.Objects;

namespace OrganisationExamenViewModel.Importation
{
    public class VisualisationViewModel : ViewModelBase
    {
        public List<ClassRoom> ClassRooms { get; set; }
        public List<Supervisor> Supervisors { get; set; }


    }
}
