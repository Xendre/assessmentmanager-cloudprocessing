﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenViewModel.ObjectsViewModel
{
    public sealed class ClassRoomViewModel : ViewModelBase
    {
        private static readonly ClassRoomViewModel instance = new ClassRoomViewModel();

        private ClassRoomViewModel() { Init(); }

        public static ClassRoomViewModel Instance
        {
            get
            {
                return instance;
            }
        }

        public  List<ClassRoomView> ClassRooms
        {
            get
            {
                return  classRooms;
            }
            set
            {
                classRooms = value;
                OnPropertyChanged("ClassRooms");

            }
        }

        private  List<ClassRoomView> classRooms;       

        private async void Init()
        {
            ClassRooms = await LoadClassRoom();
        }

        public static async Task<List<ClassRoomView>> LoadClassRoom()
        {
            var ofolder = ApplicationData.Current.LocalFolder;
            var output = ofolder.GetFileAsync("classes.xml");
            List<object> list;
            try
            {
                list = await SerializationHelper.RestoreAsync<ClassRoom>(ofolder, await output);
            }
            catch (Exception)
            {
                list = Enumerable.Empty<object>().ToList();
            }
            return (from t1 in list.Cast<ClassRoom>() select new ClassRoomView() { Num = t1.Num, Capacity = t1.Capacity, Designation = t1.Designation }).ToList();
            
        }
    }

    public class ClassRoomView
    {

        public String Num { get; set; }
        public String Designation { get; set; }
        public String Capacity { get; set; }
    }
}
