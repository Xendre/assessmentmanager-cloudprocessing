﻿using Repartition;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;

namespace TestGraph
{
    
    
    /// <summary>
    ///Classe de test pour BinaryTreeTest, destinée à contenir tous
    ///les tests unitaires BinaryTreeTest
    ///</summary>
    [TestClass]
    public class BinaryTreeTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur BinaryTree`1
        ///</summary>
        public void BinaryTreeConstructorTestHelper<T>()
        {
            BinaryTree<T> target = new BinaryTree<T>();
            Assert.IsNotNull(target);
        }

        [TestMethod]
        public void BinaryTreeConstructorTest()
        {
            BinaryTreeConstructorTestHelper<int>();
        }

        /// <summary>
        ///Test pour Clear
        ///</summary>
        public void ClearTestHelper<T>()
        {
            BinaryTree<T> target = new BinaryTree<T>(); // TODO: initialisez à une valeur appropriée
            target.Clear();
            Assert.IsNull(target.Root);
        }

        [TestMethod]
        public void ClearTest()
        {
            ClearTestHelper<int>();
        }

        /// <summary>
        ///Test pour Root
        ///</summary>
        public void RootTestHelper<T>()
        {
            BinaryTree<T> target = new BinaryTree<T>();
            BinaryTreeNode<T> expected = new BinaryTreeNode<T>(default(T));
            BinaryTreeNode<T> actual;
            target.Root = expected;
            actual = target.Root;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void RootTest()
        {
            RootTestHelper<int>();
        }
    }
}
