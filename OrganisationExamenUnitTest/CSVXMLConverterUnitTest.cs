﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Windows.Storage;
using System.Threading.Tasks;
using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Binder.Interfaces;
using OrganisationExamenModel.Services.Binder;
using System.IO;
using OrganisationExamenModel.Services.Converter;
using OrganisationExamenModel.Services.Serializer;
using OrganisationExamenModel.Services.Scheduler;

namespace OrganisationExamenUnitTest
{
    [TestClass]
    public class CSVXMLConverterUnitTest
    {
        [TestMethod]
        public async Task testTestBinder()
        {
            String data = "Nom;Duree;Type\nLatin;1h;e\nMath;2h;e\nFrançais;1h;e\nHistoire;1h;e\nSport;2h;e\nAnglais;2h;e\nMath Spe;4h;e\nHistoire;15min;o\nLatin;10h;a\nMath;2b;z\nFrançais;m10;e\nHistoire;h1;e\nSport;2h;e\nAnglais;2h;e\nMath Spe;4h;r\nHistoire;15min;g";
            String[] lines = data.Split('\n');
            IBinder<Test> binder = new TestBinder();
            List<object> tests = new List<object>();
            for (int i = 1; i < lines.Length; i++)
            {
                Test t = binder.getBindedObject(lines[i]);
                tests.Add(binder.getBindedObject(lines[i]));
            }
            StorageFile output = await ApplicationData.Current.LocalFolder.CreateFileAsync("Tests.xml",CreationCollisionOption.ReplaceExisting);
            await SerializationHelper.SaveAsync<Test>(ApplicationData.Current.LocalFolder, output, tests);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task testConverterClassRoom()
        {
            var ipath = "Testsalle.csv";
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            StorageFolder ofolder = ApplicationData.Current.LocalFolder;
            StorageFile input = await folder.GetFileAsync(ipath);
            List<ConvertError> errors = await CSVConverter.convertFile(input, ofolder);
            Assert.AreEqual(0, errors.Count);
        }
        [TestMethod]
        public async Task testConverterSupervisor()
        {
            var ipath = "testsurveillant.csv";
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            var ofolder = ApplicationData.Current.LocalFolder;
            StorageFile input = await folder.GetFileAsync(ipath);
            List<ConvertError> errors = await CSVConverter.convertFile(input, ofolder);
            Assert.AreEqual(0, errors.Count);
        }
        [TestMethod]
        public async Task testCompletImport()
        {
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            IEnumerable<StorageFile> files = null;
            List<object> sts ;
            if(folder!=null)
                files = await folder.GetFilesAsync();
            if (files != null && files.Count() >0)
            {
                var csvfiles = (from t1 in files where t1.FileType.ToLower() == ".csv" select t1);
                foreach (var csvfile in csvfiles)
                {
                    var ofolder = ApplicationData.Current.LocalFolder;
                    var a = (await CSVConverter.convertFile(csvfile, ofolder));
                }
                StorageFile xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("student.xml");
                sts = await SerializationHelper.RestoreAsync<Student>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<Student> students = sts.Cast<Student>();
                xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("test.xml");
                sts = await SerializationHelper.RestoreAsync<Test>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<Test> tests = sts.Cast<Test>();
                xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("classes.xml");
                sts = await SerializationHelper.RestoreAsync<ClassRoom>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<ClassRoom> classerooms = sts.Cast<ClassRoom>();
                xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("supervisor.xml");
                sts = await SerializationHelper.RestoreAsync<Supervisor>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<Supervisor> supervisors = sts.Cast<Supervisor>();
                DateTime start;
                DateTime.TryParse("02/18/2013 8:00:00",out start);
                DateTime end = new DateTime(start.Year,start.Month,start.Day+4,19,0,0);
                List<Test> tts = tests.ToList<Test>();
                List<ClassRoom> cms = classerooms.ToList<ClassRoom>();
                Scheduler scheduler = new Scheduler(tts,cms, start, end);
                String result = "";
                foreach (ClassRoom mroom in classerooms.ToList<ClassRoom>())
                {
                    result += mroom.ToString() + "\n";
                }
                Assert.AreEqual(36532,result.Length);
            }
            else
            {
                Assert.IsNull(files,"Files Nul");
            }
        }
        [TestMethod]
        public void testRepartition()
        {
            List<Test> tests = new List<Test>();
            List<Student> students = new List<Student>();
            Student s1 = new Student();
            s1.Birthday = "10-12-2012";
            s1.ClassRoomName="AL1";
            s1.FirstName="Xendre";
            s1.LastName="Pacif";
            s1.InternNumber="X86";
            s1.Section="AL";
            Student s2 = new Student();
            s2.Birthday = "10-12-2012";
            s2.ClassRoomName="AL2";
            s2.FirstName="Xendre2";
            s2.LastName="Pacif";
            s2.InternNumber="X87";
            s2.Section="AL";
            students.Add(s1);
            students.Add(s2);
            Test t1 = new Test();
            t1.Duration = 2;
            t1.Id = "id0";
            t1.Name = "Math";
            t1.Students = students;
            t1.Type = "Brevet";
            Test t2 = new Test();
            t2.Duration = 1;
            t2.Id = "id1";
            t2.Name = "FR";
            t2.Students = students;
            t2.Type = "Brevet";
            tests.Add(t1);
            tests.Add(t2);
            List<ClassRoom> rooms = new List<ClassRoom>();
            ClassRoom room = new ClassRoom();
            room.Capacity = "12";
            room.Designation = "Placard";
            room.Num = "21";
            ClassRoom room2 = new ClassRoom();
            room.Capacity = "12";
            room.Designation = "SalleSVT";
            room.Num = "22";
            rooms.Add(room2);
            Scheduler scheduler = new Scheduler();
            DateTime start;
            DateTime.TryParse("02/18/2013 8:00:00",out start);
            DateTime end = new DateTime(start.Year,start.Month,start.Day+4,19,0,0);
            
            Assert.AreNotEqual(null,scheduler.getRoomsSched(tests,rooms, start, end));
        }
    }
}
