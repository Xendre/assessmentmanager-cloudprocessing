﻿using Repartition;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;

namespace TestGraph
{
    
    
    /// <summary>
    ///Classe de test pour NodeListTest, destinée à contenir tous
    ///les tests unitaires NodeListTest
    ///</summary>
    [TestClass()]
    public class NodeListTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur NodeList`1
        ///</summary>
        public void NodeListConstructorTestHelper<T>()
        {
            int initialSize = 0;
            NodeList<T> target = new NodeList<T>(initialSize);
            Assert.AreEqual(0, target.Count);
        }

        [TestMethod()]
        public void NodeListConstructorTest()
        {
            NodeListConstructorTestHelper<int>();
        }

        /// <summary>
        ///Test pour Constructeur NodeList`1
        ///</summary>
        public void NodeListConstructorTest1Helper<T>()
        {
            NodeList<T> target = new NodeList<T>();
            Assert.IsNotNull(target);
        }

        [TestMethod()]
        public void NodeListConstructorTest1()
        {
            NodeListConstructorTest1Helper<int>();
        }

        /// <summary>
        ///Test pour FindByValue
        ///</summary>
        public void FindByValueTestHelper<T>()
        {
            NodeList<T> target = new NodeList<T>();
            T value = default(T);
            Node<T> n = new Node<T>(value);
            target.Add(n);
            Node<T> expected = n;
            Node<T> actual;
            actual = target.FindByValue(value);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindByValueTest()
        {
            FindByValueTestHelper<int>();
        }
    }
}
