﻿using Repartition.Graph;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using Repartition;
using System.Collections.Generic;

namespace TestGraph
{
    
    
    /// <summary>
    ///Classe de test pour GraphNodeTest, destinée à contenir tous
    ///les tests unitaires GraphNodeTest
    ///</summary>
    [TestClass]
    public class GraphNodeTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur GraphNode`1
        ///</summary>
        public void GraphNodeConstructorTestHelper<T>()
        {
            T value = default(T);
            NodeList<T> neighbors = new NodeList<T>();
            neighbors.Add(new Node<T>(default(T)));
            neighbors.Add(new Node<T>(default(T)));
            GraphNode<T> target = new GraphNode<T>(value, neighbors);
            Assert.AreEqual(2, target.Neighbors.Count);
            Assert.AreEqual(default(T), target.Value);
        }

        [TestMethod()]
        public void GraphNodeConstructorTest()
        {
            GraphNodeConstructorTestHelper<int>();
        }

        /// <summary>
        ///Test pour Constructeur GraphNode`1
        ///</summary>
        public void GraphNodeConstructorTest1Helper<T>()
        {
            T value = default(T); // TODO: initialisez à une valeur appropriée
            GraphNode<T> target = new GraphNode<T>(value);
            Assert.AreEqual(default(T), target.Value);
        }

        [TestMethod()]
        public void GraphNodeConstructorTest1()
        {
            GraphNodeConstructorTest1Helper<int>();
        }

        /// <summary>
        ///Test pour Constructeur GraphNode`1
        ///</summary>
        public void GraphNodeConstructorTest2Helper<T>()
        {
            GraphNode<T> target = new GraphNode<T>();
            Assert.IsNotNull(target);
        }

        [TestMethod()]
        public void GraphNodeConstructorTest2()
        {
            GraphNodeConstructorTest2Helper<int>();
        }

        /// <summary>
        ///Test pour Costs
        ///</summary>
        public void CostsTestHelper<T>()
        {
            GraphNode<T> target = new GraphNode<T>();
            List<int> actual;
            actual = target.Costs;
            Assert.AreEqual(0, target.Costs.Count);
        }

        [TestMethod()]
        public void CostsTest()
        {
            CostsTestHelper<int>();
        }

        /// <summary>
        ///Test pour Neighbors
        ///</summary>
        public void NeighborsTestHelper<T>()
        {
            GraphNode<T> target = new GraphNode<T>();
            NodeList<T> actual;
            actual = target.Neighbors;
            Assert.IsNotNull(actual);
        }

        [TestMethod()]
        public void NeighborsTest()
        {
            NeighborsTestHelper<int>();
        }
    }
}
