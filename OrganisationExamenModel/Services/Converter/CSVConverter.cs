﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public class CSVConverter
    {
        /// <summary>
        /// Convert CSV file to XML with syntaxe et coherencie validation
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public async static Task<List<ConvertError>> convertFile(StorageFile input, StorageFolder output)
        {
            IFileConverter converter;
            IValidator validator;
            using (var randomAccessStream = await input.OpenAsync(FileAccessMode.Read))
            {
                using (var inputStream = randomAccessStream.GetInputStreamAt(0))
                {
                    using (StreamReader streamReader = 
                       new StreamReader(inputStream.AsStreamForRead()))
                    {
                        while(streamReader.Peek()>-1)
                        {
                            String line = await streamReader.ReadLineAsync();
                            if (line.Equals("Nom;Prenom;Date de naissance;Section;Nom classe;num Interne"))
                            {
                                converter = new StudentCSVToXMLConverter();
                                validator = new StudentCSVValidator();
                                StorageFile storage =await output.CreateFileAsync("student.xml",CreationCollisionOption.ReplaceExisting);
                                return await converter.convert(input,storage, validator);
                            }
                            if (line.Equals("Nom;Duree;Type"))
                            {
                                converter = new TestCSVToXMLConverter();
                                validator = new TestCSVValidator();
                                StorageFile storage = await output.CreateFileAsync("test.xml", CreationCollisionOption.ReplaceExisting);
                                return await converter.convert(input, storage, validator);
                            }
                            if (line.Equals("Num salle;Designation;nb places"))
                            {
                                converter = new ClassRoomCSVToXMLConverter();
                                validator = new ClassRoomCSVValidator();
                                StorageFile storage = await output.CreateFileAsync("classes.xml", CreationCollisionOption.ReplaceExisting);
                                return await converter.convert(input, storage, validator);
                            }
                            if (line.Equals("Nom;Prenom;Civilite;Matiere;etablissement"))
                            {
                                converter = new SupervisorCSVToXMLConverter();
                                validator = new SupervisorCSVValidator();
                                StorageFile storage = await output.CreateFileAsync("supervisor.xml", CreationCollisionOption.ReplaceExisting);
                                return await converter.convert(input, storage, validator);
                            }
                        }
                     }
                  }
             }
            return null;
        }
    }
}
