﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public class ConverterFactory : IConvertertFactory
    {
        /// <summary>
        /// Return a FileConverter, corresponding to the argument ConverterType
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IFileConverter getConverter(ConverterType type)
        {
            switch(type){
                case ConverterType.STUDENTCSVTOXML:
                    return new StudentCSVToXMLConverter();
                case ConverterType.SUPERVISORCSVTOML:
                    return new SupervisorCSVToXMLConverter();
                case ConverterType.CLASSROMMCSVTOXML:
                    return new ClassRoomCSVToXMLConverter();
                case ConverterType.TESTCSVTOXML:
                    return new StudentCSVToXMLConverter();
                default: return null;
            }  
        }
    }
}
