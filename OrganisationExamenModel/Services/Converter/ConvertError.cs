﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public class ConvertError
    {
        public enum ErrorType
        {
            ELEMENTS_ARE_MISSING,
            SYNTAX_ERROR,
            WRONG_FILE_FORMAT
        }

        public int Line{ get; set; }
        public int Row { get; set; }
        public ErrorType type { get; set; }
        public String ExceptionMessage { get; set; }

        public ConvertError(int Line, int Row,ErrorType  type, String exceptionMessage)
        {
            this.Line = Line;
            this.Row = Row;
            this.type = type;
            ExceptionMessage = exceptionMessage;
        }

        override public String ToString()
        {
            return type.ToString()+" Error (line:"+Line+" row:"+Row+")";
        }
    }
}
