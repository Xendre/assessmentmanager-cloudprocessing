﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Binder.Interfaces
{
    /// <summary>
    /// Bind string input in a new T Object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBinder<T>
    {
        T getBindedObject(String input);
    }
}
