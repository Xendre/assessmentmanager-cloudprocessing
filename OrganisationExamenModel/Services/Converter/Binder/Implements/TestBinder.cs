﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Binder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Binder
{
    public class TestBinder:IBinder<Test>
    {
        public Test getBindedObject(string input)
        {
            //Nom;Prenom;Date de naissance;Section;Nom classe;num Interne
            String[] elems = input.Split(';');
            if (elems.Length == 3)
            {
                double duration;
                Double.TryParse(elems[1], out duration);
                return new Test() { Name=elems[0], Duration=duration, Type=elems[2]};
            }
            return null;
        }
    }
}
