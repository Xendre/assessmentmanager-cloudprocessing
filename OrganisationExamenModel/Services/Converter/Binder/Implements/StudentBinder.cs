﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Binder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Binder
{
    public class StudentBinder:IBinder<Student>
    {
        public Student getBindedObject(string input)
        {
            String[] elems = input.Split(';');
            if (elems.Length == 5)
            {
                return new Student(){LastName=elems[0],FirstName=elems[1],Birthday=elems[2],ClassRoomName=elems[3],Section=elems[4],InternNumber=""};
            }
            if (elems.Length == 6)
            {
                return new Student() { LastName = elems[0], FirstName = elems[1], Birthday = elems[2], ClassRoomName = elems[3], Section = elems[4], InternNumber = elems[5] };
            }
            return new Student() { LastName = "", FirstName = "", Birthday = "", ClassRoomName = "", Section = "", InternNumber = "" };
        }
    }
}
