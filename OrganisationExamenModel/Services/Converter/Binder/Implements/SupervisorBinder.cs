﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Binder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Binder
{
    public class SupervisorBinder:IBinder<Supervisor>
    {
        public Supervisor getBindedObject(string input)
        {
            String[] elems = input.Split(';');
            if (elems.Length == 5)
            {
                return new Supervisor(){LastName=elems[0],FirstName=elems[1], Civility=elems[2], Cours=elems[3], Establishment=elems[4]};
            }
            return null;
        }
    }
}
