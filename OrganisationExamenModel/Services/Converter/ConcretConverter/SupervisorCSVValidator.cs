﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public class SupervisorCSVValidator:IValidator
    {
        public bool validate(string content)
        {
            if (content.Length == 0)
                return false;
            String[] lines = content.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[0];
                if (line.Length != 0)
                {
                    String[] elems = line.Split(';');
                    for (int j = 0; j < elems.Length; j++)
                    {
                        if (elems.Length < 4)
                            return false;
                        if (elems[j].Length == 0 && j<=4)
                            return false;
                    }
                }
            }
            return true;
        }
    }
}
