﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public class ClassRoomCSVValidator:IValidator
    {
        public bool validate(string content)
        {
            if (content.Length == 0)
                return false;
            String[] lines = content.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[0];
                if (line.Length != 0)
                {
                    String[] elems = line.Split(';');
                    if (elems.Length == 3)
                    {
                        if (elems[3].Length == 0)
                            return false;
                    }
                    else return false;
                }
            }
            return true;
        }
    }
}
