﻿using OrganisationExamenModel.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public class ClassRoomCSVToXMLConverter:IFileConverter
    {
        private List<ConvertError> errors = new List<ConvertError>();

        public async Task<List<ConvertError>> convert(StorageFile input, StorageFile output, IValidator validator)
        {
            List<ConvertError> errors = new List<ConvertError>();
            List<object> rooms = new List<object>();
            using (var randomAccessStream = await input.OpenAsync(FileAccessMode.Read))
            {
                using (var inputStream = randomAccessStream.GetInputStreamAt(0))
                {
                    using (StreamReader streamReader =
                       new StreamReader(inputStream.AsStreamForRead()))
                    {
                        int i=0;
                        String line = "";
                        try
                        {
                            line = await streamReader.ReadLineAsync();
                        }
                        catch (Exception e)
                        {
                            errors.Add(new ConvertError(i, 0, ConvertError.ErrorType.WRONG_FILE_FORMAT, e.Message));
                            return errors;
                        }
                        while (streamReader.Peek() > -1)
                        {
                            line = await streamReader.ReadLineAsync();
                            i++;
                            String[] values = line.Split(';');
                            try
                            {
                                rooms.Add(new ClassRoom { Num = values[0], Designation = values[1], Capacity = values[2] });
                            }
                            catch (Exception e)
                            {
                                errors.Add(new ConvertError(i, values.Length + 1, ConvertError.ErrorType.ELEMENTS_ARE_MISSING, e.Message));
                            }
                        }
                    }
                }
            }
            await Serializer.SerializationHelper.SaveAsync<ClassRoom>(ApplicationData.Current.LocalFolder, output, rooms);
            return errors;
        }
    }
}
