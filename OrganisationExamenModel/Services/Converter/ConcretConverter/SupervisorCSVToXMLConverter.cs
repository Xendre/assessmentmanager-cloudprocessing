﻿using OrganisationExamenModel.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public class SupervisorCSVToXMLConverter:IFileConverter
    {
        private List<ConvertError> errors = new List<ConvertError>();
        public async Task<List<ConvertError>> convert(StorageFile input, StorageFile output, IValidator validator)
        {
            List<ConvertError> errors = new List<ConvertError>();
            List<object> supervisors = new List<object>();
            string text=null;
            try
            {
                text = await Windows.Storage.FileIO.ReadTextAsync(input);
            }
            catch (Exception e)
            {
                errors.Add(new ConvertError(0, 0, ConvertError.ErrorType.WRONG_FILE_FORMAT, e.Message));
                return errors;
            }
            if (!validator.validate(text))
            {
                errors.Add(new ConvertError(0, 0, ConvertError.ErrorType.SYNTAX_ERROR, ConvertError.ErrorType.SYNTAX_ERROR.ToString()));
            }
            else
            {
                String[] lines = text.Split('\n');
                text = null;
                String[] baliseName = lines[0].Split(';');

                for (int i = 1; i < lines.Length; i++)
                {
                    if (lines[i].Length != 0)
                    {
                        String[] values = lines[i].Split(';');
                        try
                        {
                            if (values.Length > 5)
                                supervisors.Add(new Supervisor { LastName = values[0], FirstName = values[1], Civility = values[2], Cours = values[3], Establishment = values[4], ID = values[5] });
                            else
                                supervisors.Add(new Supervisor { LastName = values[0], FirstName = values[1], Civility = values[2], Cours = values[3], Establishment = values[4], ID = "" });
                        }
                        catch (Exception e)
                        {
                            errors.Add(new ConvertError(i, values.Length + 1, ConvertError.ErrorType.ELEMENTS_ARE_MISSING, e.Message));
                        }
                    }
                }
                await Serializer.SerializationHelper.SaveAsync<Supervisor>(ApplicationData.Current.LocalFolder, output, supervisors);
            }
            return errors;
        }
    }
}
