﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public interface IValidator
    {
        /// <summary>
        /// Validate a line extracted from CSV
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        Boolean validate(String content);
    }
}
