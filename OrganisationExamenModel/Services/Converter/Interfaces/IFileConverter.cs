﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public interface IFileConverter
    {
        /// <summary>
        /// Convert CSV input file to XML output file, and check if the CSV is valid
        /// Return a list of error found in the CSV
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="validator"></param>
        /// <returns></returns>
        Task<List<ConvertError>> convert(StorageFile input, StorageFile output, IValidator validator);
    }
}
