﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public enum ConverterType { STUDENTCSVTOXML, SUPERVISORCSVTOML, CLASSROMMCSVTOXML, TESTCSVTOXML };
    public interface IConvertertFactory
    {
        /// <summary>
        /// Return the right converter from a Given enum Type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IFileConverter getConverter(ConverterType type);
    }
}
