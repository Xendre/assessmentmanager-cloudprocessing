﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using System.IO;
using Windows.Storage.Streams;

namespace OrganisationExamenModel.Services.Serializer
{
    public class SerializationHelper
    {
        static async Task<bool> DoesFileExistAsync(string fileName)
        {
            try
            {
                await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Save List of any Serializable object to XML
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="folder"></param>
        /// <param name="output"></param>
        /// <param name="Data"></param>
        /// <returns></returns>
        public static async Task SaveAsync<T>(StorageFolder folder,StorageFile output, List<object> Data)
        {
            StorageFile sessionFile = await folder.CreateFileAsync(output.Name, CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream sessionRandomAccess = await sessionFile.OpenAsync(FileAccessMode.ReadWrite);
            IOutputStream sessionOutputStream = sessionRandomAccess.GetOutputStreamAt(0);
            var serializer = new XmlSerializer(typeof(List<object>), new Type[] { typeof(T) });
            serializer.Serialize(sessionOutputStream.AsStreamForWrite(), Data);
            sessionRandomAccess.Dispose();
            await sessionOutputStream.FlushAsync();
            sessionOutputStream.Dispose();
        }
        /// <summary>
        /// Restore List of any Serialisable object from XML
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="folder"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public static async Task<List<object>> RestoreAsync<T>(StorageFolder folder, StorageFile output)
        {
            List<object> Data;
            StorageFile sessionFile = await folder.CreateFileAsync(output.Name, CreationCollisionOption.OpenIfExists);
            if (sessionFile == null)
            {
                return null;
            }
            IInputStream sessionInputStream = await sessionFile.OpenReadAsync();
            var serializer = new XmlSerializer(typeof(List<object>), new Type[] { typeof(T) });
            Data = (List<object>)serializer.Deserialize(sessionInputStream.AsStreamForRead());
            sessionInputStream.Dispose();
            return Data;
        }
    }
}
