﻿using OrganisationExamenModel.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.Data.Xml.Dom;

namespace OrganisationExamenModel.Services.XMLParser
{
    //From JB de FrobertVille
    class Parser
    {
        public static  XmlDocument document;
        public static List<Student> l_eleve = new List<Student> ();
        public static List<ClassRoom> l_salle = new List<ClassRoom>();
        public static List<Supervisor> l_surveillant = new List<Supervisor>();
        public static List<Test> l_epreuve = new List<Test>();

        public Parser()
        {
            
        }

       /* public static List<Student> Parser_Eleves()
        {
            try
            {
                document = new XmlDocument();
                document.LoadXml("../../eleves.xml");
            }
            catch (Exception e)
            {
                //Console.WriteLine(e);
            }
            
            XmlNode node = document.DocumentElement;
            try
            {
                XmlNodeList nodeList = node.SelectNodes("eleve");
                for (int cpt = 0; cpt < nodeList.Count; cpt++)
                {
                    Student e = new Student();
                    try
                    {
                        e.Internumber = nodeList[cpt].Attributes.Item(0).InnerText;
                    }
                    catch (Exception)
                    {
                        e.Internumber = null;
                    }

                    e.Firstname = nodeList[cpt].SelectNodes("nom").Item(0).InnerText;
                    e.Name = nodeList[cpt].SelectNodes("prenom").Item(0).InnerText;
                    e.Birthday = nodeList[cpt].SelectNodes("datedenaissance").Item(0).InnerText;
                    e.NameClassroom = nodeList[cpt].SelectNodes("nomclasse").Item(0).InnerText;
                    e.Section = nodeList[cpt].SelectNodes("section").Item(0).InnerText;


                    l_eleve.Add(e);
                }
                return l_eleve;
            }
            catch (Exception)
            {
               // Console.WriteLine(e);
                return null;
            }
            
        }

        public static List<ClassRoom> Parser_Salles()
        {
            try
            {
                document = new XmlDocument();
                document.LoadXml("../../salles.xml");
            }
            catch (Exception)
            {
                throw;
            }

            XmlNode node = document.DocumentElement;
            try
            {
                XmlNodeList nodeList = node.SelectNodes("salle");
                for (int cpt = 0; cpt < nodeList.Count; cpt++)
                {
                    ClassRoom s = new ClassRoom();
                    try
                    {
                        s.Num = nodeList[cpt].Attributes.Item(0).InnerText;
                    }
                    catch (Exception)
                    {
                        s.Num= null;
                    }

                    s.Designation = nodeList[cpt].SelectNodes("designation").Item(0).InnerText;
                    s.NbPlace = nodeList[cpt].SelectNodes("nbPlaces").Item(0).InnerText;
                    l_salle.Add(s);
                }
                return l_salle;
            }
            catch (Exception)
            {
                return null;
            }

        }

        
        public static List<Test> Parser_Epreuves()
        {
            try
            {
                document = new XmlDocument();
                document.Load("../../epreuve.xml");
            }
            catch (Exception)
            {
                throw;
            }

            XmlNode node = document.DocumentElement;
            try
            {
                XmlNodeList nodeList = node.SelectNodes("epreuve");
                for (int cpt = 0; cpt < nodeList.Count; cpt++)
                {
                    Test e = new Test();
                    try
                    {
                        e.Id = nodeList[cpt].Attributes.Item(0).InnerText;
                    }
                    catch (Exception)
                    {
                        e.Id = null;
                    }

                    e.Name = nodeList[cpt].SelectNodes("nom").Item(0).InnerText;
                    e.Duration = nodeList[cpt].SelectNodes("duree").Item(0).InnerText;
                    e.Type = nodeList[cpt].SelectNodes("type").Item(0).InnerText;
                    l_epreuve.Add(e);
                }
                return l_epreuve;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Supervisor> Parser_Surveillants()
        {
            try
            {
                document = new XmlDocument();
                document.Load("../../surveillants.xml");
            }
            catch (Exception)
            {
                throw;
            }

            XmlNode node = document.DocumentElement;
            try
            {
                XmlNodeList nodeList = node.SelectNodes("surveillant");
                for (int cpt = 0; cpt < nodeList.Count; cpt++)
                {
                    Supervisor s = new Supervisor();
                    try
                    {
                        s.Establishment = nodeList[cpt].SelectNodes("etablissement").Item(0).InnerText;
                    }
                    catch (Exception){}

                    s.FirstName = nodeList[cpt].SelectNodes("nom").Item(0).InnerText;
                    s.Name = nodeList[cpt].SelectNodes("prenom").Item(0).InnerText;
                    s.Civility = nodeList[cpt].SelectNodes("civilite").Item(0).InnerText;
                    s.Cours = nodeList[cpt].SelectNodes("matiere").Item(0).InnerText;
                    
                    l_surveillant.Add(s);
                }
                return l_surveillant;
            }
            catch (Exception)
            {
                return null;
            }
        }*/



    }
}
