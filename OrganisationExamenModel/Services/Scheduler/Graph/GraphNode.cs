﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repartition.Graph
{
    public class GraphNode<T> : Node<T>
    {
        private List<int> costs;
        public Boolean Visited { get; set; }
        public int Color { get; set; }
        public GraphNode() : base() { Color = -1; Visited = false; }
        public GraphNode(T value) : base(value) { Color = -1; Visited = false; }
        public GraphNode(T value, NodeList<T> neighbors) : base(value, neighbors) { Color = -1; Visited = false; }

        new public NodeList<T> Neighbors
        {
            get
            {
                if (base.Neighbors == null)
                    base.Neighbors = new NodeList<T>();

                return base.Neighbors;
            }
        }

        public List<int> Costs
        {
            get
            {
                if (costs == null)
                    costs = new List<int>();

                return costs;
            }
        }
    }
}
