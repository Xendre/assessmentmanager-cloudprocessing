﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrganisationExamenModel.Objects.Interfaces;
using OrganisationExamenModel.Objects;
using Repartition.Graph;
using Repartition;
namespace OrganisationExamenModel.Services.Scheduler
{
    public class Scheduler
    {
        public static int MAX_HOURS_PER_DAY = 10;
        public static int NB_DAYS = 5;
        public static double BLOCK_SIZE=0.5;
        public static int BEGIN_HOUR = 8;
        public static double BREAK_TIME = 0.25;
        Graph<ScheduleItem> schedGraph = new Graph<ScheduleItem>();
        public Scheduler()
        {
        }
        /// <summary>
        /// Constructor which set all the ScheduleItem on a specified duration
        /// </summary>
        /// <param name="tests"></param>
        /// <param name="rooms"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public Scheduler(List<Test> tests,List<ClassRoom> rooms,DateTime start, DateTime end)
        { 
            foreach (ClassRoom room in rooms)
            {
                room.SchedBlocks = initSchedList(start, end);
            }
            int k = 0;
            foreach (Test test in tests)
            {
                int nbBlock = (int)Math.Ceiling(test.Duration/BLOCK_SIZE);
                int blocksPerDay = (int)Math.Ceiling(MAX_HOURS_PER_DAY / BLOCK_SIZE);
                    Boolean flag = false;
                    for (int i = 0; i < nbBlock; i++)
                    {
                        foreach (ScheduleItem item in rooms.ElementAt(k).SchedBlocks)
                        {
                            if (!item.IsActive)
                            {
                                item.Test = test;
                                item.ClasseRoom = rooms.ElementAt(k);
                                item.IsActive = true;
                                if (i == nbBlock - 1)
                                    flag = true;
                                break;
                            }
                        }
                    }
                    if (flag == true)
                        k++;
                    if (k == rooms.Count)
                        k = 0;
            }
        }
        /// <summary>
        /// Add all possible scheduleItem to each ClassRoom
        /// </summary>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <returns></returns>
        private List<ScheduleItem> initSchedList(DateTime start, DateTime stop)
        {
            List<ScheduleItem> schedItems = new List<ScheduleItem>();
            TimeSpan elapsed;
            if(start<stop)
                elapsed = stop.Subtract(start);
            else
                elapsed = start.Subtract(stop);
            int nbDays = (int)Math.Ceiling(elapsed.TotalDays);
            int maxBlockPerDay = (int)Math.Ceiling(MAX_HOURS_PER_DAY / BLOCK_SIZE);
            DateTime dateblock = new DateTime(start.Year,start.Month,start.Day,BEGIN_HOUR,0,0);
            for (int i = 0; i < nbDays; i++)
            {
                for (int j = 0; j < maxBlockPerDay; j++)
                {
                    ScheduleItem item = new ScheduleItem();
                    item.Begin = new DateTime(dateblock.Year, dateblock.Month, dateblock.Day + i, dateblock.Hour, 0, 0);
                    item.Begin=item.Begin.AddHours(j * BLOCK_SIZE);
                    item.Begin = item.Begin.AddHours(BREAK_TIME);
                    item.End = item.Begin.AddHours(BLOCK_SIZE);
                    schedItems.Add(item);
                }
            }
            return schedItems;
        }
        /// <summary>
        /// Fill schedulItem info from Constraints, to be ready to be exploited with Schedule PDF Generator
        /// </summary>
        /// <param name="tests"></param>
        /// <param name="rooms"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<ClassRoom> getRoomsSched(List<Test> tests,List<ClassRoom> rooms, DateTime start, DateTime end)
        {
            Graph<ScheduleItem> schedGraph = new Graph<ScheduleItem>();
            foreach (ClassRoom room in rooms)
            {
                room.SchedBlocks = initSchedList(start, end);
                
                foreach (ScheduleItem block in room.SchedBlocks)
                {
                    block.ClasseRoom=room;
                    schedGraph.AddNode(block);
                }
            }
            foreach (GraphNode<ScheduleItem> node in schedGraph.Nodes)
            {
                foreach (GraphNode<ScheduleItem> node2 in schedGraph.Nodes)
                {
                    if (node.Value != node2.Value)
                    {
                        if (node.Value.ClasseRoom == node2.Value.ClasseRoom)
                        {
                            schedGraph.AddUndirectedEdge(node, node2,1);
                        }
                    }
                }
            }
            Graph<Test> testGraph = new Graph<Test>();
            foreach (Test test in tests)
            {
                testGraph.AddNode(test);
            }
            foreach (GraphNode<Test> test in testGraph.Nodes)
            {
                foreach (GraphNode<Test> test2 in testGraph.Nodes)
                {
                    if (test != test2)
                    {
                        if (test.Value.Students == test2.Value.Students)
                            testGraph.AddUndirectedEdge(test, test2,1);
                    }
                }
            }
            testGraph.Colorize();
            schedGraph.Colorize();
            return rooms;
        }
    }
   
}
