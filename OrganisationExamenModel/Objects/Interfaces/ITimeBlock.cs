﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects.Interfaces
{
    public interface ITimeBlock
    {
        [DataMember()]
        DateTime Begin { get; set; }
        [DataMember()]
        DateTime End { get; set; }
        [DataMember()]
        Boolean IsActive { get; set; }
    }
}
