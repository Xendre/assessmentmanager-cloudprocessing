﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(Test))]
    [DataContractAttribute]
    public class Test
    {
        [DataMember()]
        public String Name{get;set;}
        [DataMember()]
        public Double Duration { get; set; }
        [DataMember()]
        public String Type { get; set; }
        [DataMember()]
        public String Id { get; set; }
        [DataMember()]
        public List<Student> Students { get; set; }
    }
}
