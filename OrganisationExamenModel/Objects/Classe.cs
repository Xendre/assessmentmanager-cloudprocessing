﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    public class Classe
    {
        public String ClasseName{get; set; }
        public List<Student> Students { get; set; }
        public Classe(String name)
        {
            ClasseName = name;
        }
        public Classe(String name, List<Student> students)
        {
            ClasseName = name;
            if (students != null)
                Students = students;
            else
                Students = new List<Student>();

        }
        public Boolean addStudent(Student student)
        {
            if (Students == null)
                Students = new List<Student>();
            if (!Students.Contains(student))
            {
                Students.Add(student);
                return true;
            }
            return false;
        }
        public static List<Classe> getClasses(List<Student> students)
        {
            List<Classe> classes = new List<Classe>();
            Boolean flag = false;
            foreach (Student st in students)
            {
                foreach (Classe cl in classes)
                {
                    if (cl.ClasseName == st.Section)
                    {
                        cl.addStudent(st);
                        flag = true;
                        break;
                    }
                    else flag = false;
                }
                if (!flag)
                {
                    Classe c = new Classe(st.Section);
                    c.addStudent(st);
                    classes.Add(c);
                    flag = true;
                }
            }
            return classes;
        }
    }
}
