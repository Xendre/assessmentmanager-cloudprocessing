﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(OrganisationExamenModel.Objects.Interfaces.ITimeBlock))]
    [DataContractAttribute]
    public class TimeBlock : Interfaces.ITimeBlock
    {
        [DataMember()]
        private DateTime beginTime;
        [DataMember()]
        private DateTime endTime;
        [DataMember()]
        private Boolean isActive;

        public DateTime Begin
        {
            get
            {
                if (beginTime == null)
                {
                    beginTime = DateTime.Now;
                }
                return beginTime;
            }
            set
            {
                beginTime = value;
            }
        }
        public DateTime End
        {
            get
            {
                if (endTime == null)
                {
                    endTime = DateTime.Now;
                }
                return endTime;
            }
            set
            {
                endTime = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
    }
}
