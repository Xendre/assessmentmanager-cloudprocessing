﻿using OrganisationExamenModel.Objects.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(ScheduleItem))]
    [DataContractAttribute]
    public class ScheduleItem : TimeBlock
    {
        [DataMember()]
        public Test Test { get; set; }
        [DataMember()]
        public ClassRoom ClasseRoom { get; set; }
        [DataMember()]
        public List<Supervisor> Supervisors { get; set; }
        override public String ToString()
        {
            String result = "";
            result += "Classe Room Number: " + ClasseRoom.Num + "\n";
            result += "Classe Room Name: " + ClasseRoom.Designation + "\n";
            result += "Supervisor: Not Implemented"+"\n";
            result += "Test : " + Test.Name + "\n";
            result += "Start : " + this.Begin.DayOfWeek+" "+this.Begin.Day+" "+this.Begin.Hour+"h"+this.Begin.Minute + "\n";
            result += "End : " + this.End.DayOfWeek + " " + this.End.Day + " " + this.End.Hour + "h" + this.End.Minute + "\n";
            foreach (Student stud in Test.Students)
            {
                result += stud.FirstName + " " + stud.LastName + "\n";
            }
            return result;
        }
    }
}
