﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(Student))]
    [DataContractAttribute]
    public class Student
    {
        [DataMember()]
        public String FirstName { get; set; }
        [DataMember()]
        public String LastName { get; set; }
        [DataMember()]
        public String Birthday { get; set; }
        [DataMember()]
        public String ClassRoomName { get; set; }
        [DataMember()]
        public String InternNumber { get; set; }
        [DataMember()]
        public String Section{ get; set; }

    }
}
