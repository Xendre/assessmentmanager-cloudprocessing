﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(OrganisationExamenModel.Objects.Import))]
    [DataContractAttribute]
    class Import
    {
        [DataMember()]
        List<ClassRoom> classeRooms;
        [DataMember()]
        List<Student> students;
        [DataMember()]
        List<Supervisor> supervisors;
        [DataMember()]
        List<Test> tests;

        public Import()
        {
            classeRooms = new List<ClassRoom>();
            students = new List<Student>();
            supervisors = new List<Supervisor>();
            tests = new List<Test>();
        }

        public Import(List<ClassRoom> classeRooms, List<Student> students, List<Supervisor> supervisors, List<Test> tests)
        {
            this.classeRooms = classeRooms;
            this.students = students;
            this.supervisors = supervisors;
            this.tests = tests;
        }

        public void addClassRoom(ClassRoom classe)
        {
            classeRooms.Add(classe);
        }
        public void addStudent(Student student)
        {
            students.Add(student);
        }
        public void addSupervisor(Supervisor supervisor)
        {
            supervisors.Add(supervisor);
        }
        public void addTest(Test test)
        {
            tests.Add(test);
        }
    }
}
