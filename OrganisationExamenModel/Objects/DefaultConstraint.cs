﻿using OrganisationExamenModel.Objects.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    public class DefaultConstraint : IConstraint
    {
        public ITimeBlock TimeRange { get; set; }

        public Boolean isCompatible(IConstraint constraint)
        {
            if (TimeRange.IsActive != constraint.TimeRange.IsActive)
                return false;
            return TimePeriodOverlap(TimeRange.Begin, TimeRange.End, constraint.TimeRange.Begin, constraint.TimeRange.End);
        }

        private bool TimePeriodOverlap(DateTime BS, DateTime BE, DateTime TS, DateTime TE)
        {
            return (
                (TS >= BS && TS < BE) || (TE <= BE && TE > BS) || (TS <= BS && TE >= BE)
            );
        }
    }
}
