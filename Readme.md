AssessmentManager - Documentation
=================================
AssessmentManager is an assessement AUTOMATIC Scheduler.
From RAW CSV file, it produces Room/Student/Supervisor Schedule Item.
It can be export as PDF (Soon)

System equirements
------------------
* Visual Studio 2012
* .NET4.5
* Windows 8

Installation
------------
* Clone the project repository
* Open the visual studio project (*.sln)
* Compile the projet in Debug or Release mode
* Open the Application Tile in ModernUI

Unit Testing Problem (weird case!)
------------

* Compile the projet
* Run all UnitTest
* If some Test throw filenotfound Exception, check if (*.csv are present in the TargetDir\Appx folder)
* If not Clean the generated File ==> Generate->Clean
* Build the project Again
* Run the test again, it should do the trick
* If not, copy *csv from SolutionDir\Jeux de test\ to TargetDir\Appx\